
import processing.video.*;

Capture cam;

PImage img1, img2;
int THRESHOLD = 60;

int width = 1280;
int height = 720;


void setup() {
  size(width, height);
  img1 = loadImage("bg.jpg");
  img2 = loadImage("bg.jpg");

  String[] cameras = Capture.list();
  
  if (cameras.length == 0) {
    println("There are no cameras available for capture.");
    exit();
  } else {
    println("Available cameras:");
    for (int i = 0; i < cameras.length; i++) {
      println(cameras[i]);
    }
    
    // The camera can be initialized directly using an 
    // element from the array returned by list():
    cam = new Capture(this, cameras[0]);
    cam.start();     
  }
}


void draw() {
  if (cam.available() == true) {
    //cam.read();
    //cam.updatePixels();
    cam.read();
    img1.copy(cam, 0, 0, cam.width, cam.height, 0, 0, cam.width, cam.height);
    img1.updatePixels();
  }

  background(150);
  img1.loadPixels();
  PImage screenImg = createImage(width, height, RGB);
  int mousePixel = mouseY*screenImg.width+mouseX;
  
  
  color mouseColor = img1.pixels[mousePixel];
  float mRed = red(mouseColor);
  float mGreen = green(mouseColor);
  float mBlue = blue(mouseColor);

  for (int i = 0; i < img1.pixels.length; i++) {
    color pixelColor = img1.pixels[i];
    float r = red(pixelColor);
    float g = green(pixelColor);
    float b = blue(pixelColor);
    
    float colorDifference = abs(mRed - r) + abs(mGreen - g) + abs(mBlue - b);
    
    if (colorDifference < THRESHOLD) {    
      screenImg.pixels[i] = img2.pixels[i];
    }else{
      screenImg.pixels[i] = img1.pixels[i];
    }
  }
  screenImg.updatePixels();
  image(screenImg, 0, 0);
  //image(cam, 0, 0);
}

