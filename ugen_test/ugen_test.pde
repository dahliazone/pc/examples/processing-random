/**
  * This sketch demonstrates how to create a simple synthesis chain that 
  * involves controlling the value of a UGenInput with the output of 
  * a UGen. In this case, we patch an Oscil generating a sine wave into 
  * the amplitude input of an Oscil generating a square wave. The result 
  * is known as amplitude modulation.
  * <p>
  * For more information about Minim and additional features, 
  * visit http://code.compartmental.net/minim/
  */

import ddf.minim.*;
import ddf.minim.ugens.*;

Minim minim;
AudioOutput out;
//Oscil       wave;

Oscil       sine;
float sineHz = 2;
float sineAmplitude = 0.4f;

Oscil       triangle;
float triangleHz = 440;
float triangleAmplitude = 1.0f;


void setup()
{
  size(512, 200, P3D);
  
  minim = new Minim(this);
  
  // use the getLineOut method of the Minim object to get an AudioOutput object
  out = minim.getLineOut();

  // create a triangle wave Oscil, set to 440 Hz, at 1.0 amplitude
  // in this case, the amplitude we construct the Oscil with 
  // doesn't matter because we will be patching something to
  // its amplitude input.
  triangle = new Oscil( 440, 1.0f, Waves.TRIANGLE );
 
  // create a sine wave Oscil for modulating the amplitude of wave
  sine  = new Oscil( sineHz, sineAmplitude, Waves.SINE );
 
  // connect up the modulator
  //mod.patch( wave.amplitude );
  
  // patch wave to the output
  sine.patch( out );
  triangle.patch( out );
}


void draw()
{
  background(0);
  stroke(255);
  
  text("sine hz="+sineHz, 10, 10);
  text("sine amplitude="+sineAmplitude, 10, 30);

  text("triangle hz="+triangleHz, 10, 50);
  text("triangle amplitude="+triangleAmplitude, 10, 70);
  
  // draw the waveforms
  for(int i = 0; i < out.bufferSize() - 1; i++)
  {
    line( i, 50 + out.left.get(i)*50, i+1, 50 + out.left.get(i+1)*50 );
    line( i, 150 + out.right.get(i)*50, i+1, 150 + out.right.get(i+1)*50 );
  }
}



void mouseMoved()
{
  float pan = map( mouseX, 0, width, -1, 1 );
  float filter = map( mouseY, 0, height, -1, 1 );
  //sine.setPan( pan );
  //saw.setPan( -pan );

  //out.setPan( pan );
  //wave.setAmplitude(440*pan);

  // sine instrument
  sineHz = map( mouseY, 0, height, 0, 440 );
  sineAmplitude = map( mouseX, 0, width, 0, 2 );
  sine.setFrequency(sineHz);
  sine.setAmplitude(sineAmplitude);

  triangleHz = map( mouseY, 0, height, 0, 440 );
  triangleAmplitude = map( mouseX, 0, width, 0, 1 );
  triangle.setFrequency(triangleHz);
  triangle.setAmplitude(triangleAmplitude);
}

